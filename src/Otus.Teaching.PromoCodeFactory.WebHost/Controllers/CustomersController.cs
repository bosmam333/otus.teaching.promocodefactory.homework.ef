﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private IRepository<Customer> _customerRepository;
        private IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository,
                                   IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary> Получить всех клиентов </summary>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var data = await _customerRepository.GetAllAsync();

            var responseModel = data.Select(x =>
            {
                return new CustomerShortResponse()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email
                };
            });

            return Ok(responseModel);
        }

        /// <summary> Получить клиента по id </summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var data = await _customerRepository.GetByIdAsync(id);

            if (data == null)
                return NotFound();

            var response = new CustomerResponse()
            {
                Id = data.Id,
                FirstName = data.FirstName,
                LastName = data.LastName,
                Email = data.Email,
                Preferences = new List<PreferenceResponse>()
            };

            response.Preferences = data.CustomerPreferences.Select(x => new PreferenceResponse()
            {
                Id = x.PreferenceId,
                Name = x.Preference.Name
            }).ToList();

            return Ok(response);
        }

        /// <summary> Создать клиента </summary>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var customer = new Customer()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreferences = new List<CustomerPreference>()
            };

            var preferences = await _preferenceRepository.GetAllAsync();

            var customerPreferences = preferences.Where(x => request.PreferenceIds.Contains(x.Id)).ToList();
            foreach (var customerPreference in customerPreferences)
            {
                customer.CustomerPreferences.Add(new CustomerPreference()
                {
                    Customer = customer,
                    Preference = customerPreference
                });
            }

            await _customerRepository.CreateAsync(customer);

            var response = new CustomerShortResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = request.Email
            };

            return Ok(response);
        }

        /// <summary> Обновить клиента </summary>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomerAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var preferences = await _preferenceRepository.GetAllAsync();
            var customerPreferences = preferences.Where(x => request.PreferenceIds.Contains(x.Id)).ToList();

            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                var customerNew = new Customer()
                {
                    Id = id,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Email = request.Email,
                    CustomerPreferences = new List<CustomerPreference>()
                };

                foreach (var customerPreference in customerPreferences)
                {
                    customerNew.CustomerPreferences.Add(new CustomerPreference()
                    {
                        Customer = customerNew,
                        Preference = customerPreference
                    });
                }

                await _customerRepository.CreateAsync(customerNew);

                return StatusCode(201);
            }

            customer.Id = id;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.CustomerPreferences.Clear();
            foreach (var customerPreference in customerPreferences)
            {
                customer.CustomerPreferences.Add(new CustomerPreference()
                {
                    Customer = customer,
                    Preference = customerPreference
                });
            }

            await _customerRepository.UpdateAsync(customer);

            return StatusCode(200);
        }

        /// <summary> Удалить клиента </summary>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            if(await _customerRepository.DeleteAsync(id))
                return StatusCode(204);

            return NotFound();
        }
    }
}