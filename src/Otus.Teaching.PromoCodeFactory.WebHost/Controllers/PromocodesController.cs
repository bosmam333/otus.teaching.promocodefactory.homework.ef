﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private IRepository<PromoCode> _promocodeRepository;
        private IRepository<Preference> _preferenceRepository;
        private IRepository<Customer> _customerRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository,
                                    IRepository<Preference> preferenceRepository,
                                    IRepository<Customer> customerRepository)
        {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;

        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var data = await _promocodeRepository.GetAllAsync();

            var responseModel = data.Select(x =>
            {
                return new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.Info,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName
                };
            });

            return Ok(responseModel);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var preference = await _preferenceRepository.GetByIdAsync(request.PreferenceId);
            var customerPreferences = preference.CustomerPreferences;

            foreach (var customerPreference in customerPreferences)
            {
                var promocode = new PromoCode()
                {
                    Id = Guid.NewGuid(),
                    Code = request.PromoCode,
                    Info = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    Customer = customerPreference.Customer,
                    Preference = preference
                };

                await _promocodeRepository.CreateAsync(promocode);
            }

            return Ok();
        }
    }
}