﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        [MaxLength(200)]
        public string ServiceInfo { get; set; }

        [MaxLength(30)]
        public string PartnerName { get; set; }

        [MaxLength(20)]
        public string PromoCode { get; set; }

        public Guid PreferenceId { get; set; }
    }
}