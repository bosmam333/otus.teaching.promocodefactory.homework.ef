﻿using System;
namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public interface IEfDbInitializer
    {
        void Initialize();
    }
}
