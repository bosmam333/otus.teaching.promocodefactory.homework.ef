﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer: IEfDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext _dataContext)
        {
            this._dataContext = _dataContext;
        }

        public void Initialize()
        {
            if (_dataContext.Customers.ToList().Count == 0)
            {
                _dataContext.AddRange(FakeDataFactory.Preferences);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Customers);
                _dataContext.SaveChanges();

                _dataContext.AddRange(FakeDataFactory.Employees);
                _dataContext.SaveChanges();
            }
        }
    }
}
