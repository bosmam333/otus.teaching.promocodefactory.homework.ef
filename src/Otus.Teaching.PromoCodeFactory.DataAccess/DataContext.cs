﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

public class DataContext: DbContext
{
    public DbSet<Role> Roles { get; set; }
    public DbSet<Preference> Preferences { get; set; }
    public DbSet<Employee> Employees { get; set; }
    public DbSet<Customer> Customers { get; set; }

    public DataContext()
    {
    }

    public DataContext(DbContextOptions<DataContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // у каждого клиента есть набор предпочтений
        // у каждого предпочтения есть набор клиентов
        modelBuilder.Entity<CustomerPreference>()
             .HasKey(x => new { x.CustomerId, x.PreferenceId });
        modelBuilder.Entity<CustomerPreference>()
             .HasOne(x => x.Customer)
             .WithMany(x => x.CustomerPreferences)
             .HasForeignKey(x => x.CustomerId);
        modelBuilder.Entity<CustomerPreference>()
             .HasOne(x => x.Preference)
             .WithMany(x => x.CustomerPreferences)
             .HasForeignKey(x => x.PreferenceId);

        // у каждой роли свой набор сотрудников. У сотрудника одновременно только одна роль
        modelBuilder.Entity<Employee>()
            .HasOne(x => x.Role)
            .WithMany()
            .HasForeignKey(x => x.RoleId);

        // у каждого предпочтения свой набор промокодов
        modelBuilder.Entity<PromoCode>()
            .HasOne(x => x.Preference)
            .WithMany()
            .HasForeignKey(x => x.PreferenceId);

        // у каждого клиента свой набор промокодов. Один промокод может быть только у одного клиента
        modelBuilder.Entity<PromoCode>()
            .HasOne(x => x.Customer)
            .WithMany(x => x.PromoCodes)
            .HasForeignKey(x => x.CustomerId);
    }
}